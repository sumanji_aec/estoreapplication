package com.estore.estore.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ES_PRODUCT")
public class Product {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ES_PRODUCT_ID")
	private int productId;
	
	@Column(name="ES_PRODUCT_NAME")
	private String productName;
	
	@Column(name="ES_PRODUCT_PRICE")
	private double productPrice;
	
	@Column(name="ES_PRODUCT_AVAILABLE", columnDefinition="boolean default true")
	private boolean productAvailable;
	
	

	/**
	 * @return the productId
	 */
	public int getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(int productId) {
		this.productId = productId;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName the productName to set
	 */
	public void setProductName(String productName) {
		this.productName = productName;
	}

	/**
	 * @return the productPrice
	 */
	public double getProductPrice() {
		return productPrice;
	}

	/**
	 * @param productPrice the productPrice to set
	 */
	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}

	/**
	 * @return the productAvailable
	 */
	public boolean isProductAvailable() {
		return productAvailable;
	}

	/**
	 * @param productAvailable the productAvailable to set
	 */
	public void setProductAvailable(boolean productAvailable) {
		this.productAvailable = productAvailable;
	}

	
	
	
}

