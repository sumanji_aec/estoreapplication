package com.estore.estore.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.estore.estore.model.Category;
import com.estore.estore.model.Product;
import com.estore.estore.service.CategoryService;
import com.estore.estore.service.ProductService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Contact;
import io.swagger.annotations.Info;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.v3.oas.annotations.parameters.RequestBody;

@RestController
public class EstoreCategoryController {

	private ProductService productService;
	private CategoryService categoryService;
	
	@ApiOperation(value="Get All Categories")
	@RequestMapping(value="/categories",method=RequestMethod.GET)
	private List<Category> getAllCategories(){
		return categoryService.getAllCategory();
	}
	
	@ApiOperation(value="Get  Category by Id")
	@RequestMapping(value="/categories/{id}",method=RequestMethod.GET)
	private Category getCategory(@PathVariable(value="id") int categoryId){
		Category responseCategory= null;
		Optional<Category> category=categoryService.getCategoryById(categoryId);
		if(category.isPresent()){
			responseCategory=category.get();
		}
		 return responseCategory;
	}
	
	@ApiOperation(value="Add Category")
	@PostMapping(value="/category")
	private void addCategory(@RequestBody Category category){
		if(category!=null){
			categoryService.addCategory(category);
		}
	}
	
	@ApiOperation(value="Get All Products")
	@RequestMapping(value="/products",method=RequestMethod.GET)
	private List<Product> getAllProducts(){
		return productService.getAllProducts();
	}
	
	@ApiOperation(value="Get Product by Id")
	@RequestMapping(value="/product/{id}",method=RequestMethod.GET)
	private Product getProduct(@PathVariable(value="id") int productId){
		Product responseProduct= null;
		Optional<Product> product=productService.getProductById(productId);
		if(product.isPresent()){
			responseProduct=product.get();
		}
		 return responseProduct;
	}
	
	@ApiOperation(value="Add Product")
	@PostMapping(value="/product")
	private void addProduct(@RequestBody Product product){
		if(product!=null){
			productService.addProduct(product);
		}
	}
	
	
	
	
	/**
	 * @return the productService
	 */
	public ProductService getProductService() {
		return productService;
	}
	/**
	 * @param productService the productService to set
	 */
	@Autowired
	public void setProductService(ProductService productService) {
		this.productService = productService;
	}
	/**
	 * @return the categoryService
	 */
	public CategoryService getCategoryService() {
		return categoryService;
	}
	/**
	 * @param categoryService the categoryService to set
	 */
	@Autowired
	public void setCategoryService(CategoryService categoryService) {
		this.categoryService = categoryService;
	}
	
}
