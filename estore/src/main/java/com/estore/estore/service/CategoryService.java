package com.estore.estore.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.estore.estore.dao.EStoreCategoryRepository;
import com.estore.estore.model.Category;

@Service
public class CategoryService {

	private EStoreCategoryRepository categoryDB;

	
	
	public Optional<Category> getCategoryById(int categoryId){
		return categoryDB.findById(categoryId);
	}
	
	public List<Category> getAllCategory(){
		return (List<Category>) categoryDB.findAll();
	}
	
	public void addCategory(Category category){
		categoryDB.save(category);
		
	}
	
	public void addAllCategory(List<Category> categories){
		categoryDB.saveAll(categories);
	}
	
	public void removeCategory(int categoryId){
		 categoryDB.deleteById(categoryId);
	}	
	
	
	/**
	 * @return the eStoreDao
	 */
	public EStoreCategoryRepository geteStoreDao() {
		return categoryDB;
	}

	/**
	 * @param eStoreDao the eStoreDao to set
	 */
	@Autowired
	public void seteStoreDao(EStoreCategoryRepository eStoreDao) {
		this.categoryDB = eStoreDao;
	}
	
}
