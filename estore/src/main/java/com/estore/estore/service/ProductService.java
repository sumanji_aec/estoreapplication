package com.estore.estore.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.estore.estore.dao.EStoreProductRepository;
import com.estore.estore.model.Product;

@Service
public class ProductService {

	private EStoreProductRepository productDB;

	public Optional<Product> getProductById(int productId){
		return productDB.findById(productId);
	}
	
	public List<Product> getAllProducts(){
		return (List<Product>) productDB.findAll();
	}
	
	public void addProduct(Product product){
		productDB.save(product);
		
	}
	
	public void addAllProduct(List<Product> products){
		productDB.saveAll(products);
	}
	
	public void removeProduct(int productId){
		 productDB.deleteById(productId);
	}

	/**
	 * @return the productDB
	 */
	public EStoreProductRepository getProductDB() {
		return productDB;
	}

	/**
	 * @param productDB the productDB to set
	 */
	@Autowired
	public void setProductDB(EStoreProductRepository productDB) {
		this.productDB = productDB;
	}
	
}
