package com.estore.estore.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.estore.estore.model.Category;

@Repository
public interface EStoreCategoryRepository extends CrudRepository<Category, Integer> {
}
