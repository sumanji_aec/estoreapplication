package com.estore.estore.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.estore.estore.model.Category;
import com.estore.estore.model.Product;

@Repository
public interface EStoreProductRepository extends CrudRepository<Product, Integer> {
}
